package com.mallplus.common.entity.jifen.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mallplus.common.entity.jifen.entity.JifenDonateRule;


/**
 * @author mallplus
 * @date 2019-12-17
 */
public interface JifenDonateRuleMapper extends BaseMapper<JifenDonateRule> {
}
